//
// "$Id: FavoritesMenu.cxx 386 2006-03-04 14:15:27Z mike $"
//
// FavoritesMenu widget code.
//
// Copyright 2005 by Michael Sweet.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License v2 as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// Contents:
//
//   FavoritesMenu::add_favorite() - Add saved directory.
//   FavoritesMenu::handle()       - Handle UI events.
//   Favorites::load_menu()        - Load the current directory favorites.
//   FavoritesMenu::quote()        - Quote a filename for a menu item.
//   FavoritesMenu::unquote()      - Unquote a pathname from a menu.
//

#include "FavoritesMenu.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//
// 'FavoritesMenu::add_favorite()' - Add saved directory.
//

void
FavoritesMenu::add_favorite(const char *d)
					// I - Directory to add
{
  int			i;		// Looping var
  char			name[255],	// Preference name
			filename[1024];	// Filename
  Fl_Preferences	prefs(Fl_Preferences::USER, "fltk.org", "filechooser");
					// File chooser preferences


  // See if directory is already in favorites...
  for (i = 0; i < 100; i ++)
  {
    sprintf(name, "favorite%02d", i);

    prefs.get(name, filename, "", sizeof(filename));

    if (!filename[0])
      break;

    if (!strcmp(filename, d))
      return;
  }

  prefs.set(name, d);
}


//
// 'FavoritesMenu::handle()' - Handle UI events.
//

int					// O - 1 if handled, 0 otherwise
FavoritesMenu::handle(int event)	// I - Event
{
  // Reload the menu when a mouse button or key is pressed...
  if (event == FL_PUSH || event == FL_SHORTCUT)
    load_menu();

  // Now pass the event to the menu button widget...
  return (Fl_Menu_Button::handle(event));
}


//
// 'Favorites::load_menu()' - Load the current directory favorites.
//

void
FavoritesMenu::load_menu()
{
  int			i;		// Looping var
  char			name[255],	// Preference name
			filename[1024],	// Filename
			menuitem[2048];	// Menu item
  const char		*home;		// Home directory
  Fl_Preferences	prefs(Fl_Preferences::USER, "fltk.org", "filechooser");
					// File chooser preferences


  // Clear the list 
  clear();
  add("Add to Favorites", FL_ALT + 'a', 0);
  add("Manage Favorites", FL_ALT + 'm', 0, 0, FL_MENU_DIVIDER);
  add("Filesystems", FL_ALT + 'f', 0);

  if ((home = getenv("HOME")) != NULL)
  {
    quote(menuitem, home, sizeof(menuitem));
    add(menuitem, FL_ALT + 'h', 0);
  }

  for (i = 0; i < 100; i ++)
  {
    sprintf(name, "favorite%02d", i);
    prefs.get(name, filename, "", sizeof(filename));
    if (!filename[0]) break;

    quote(menuitem, filename, sizeof(menuitem));

    add(menuitem, i < 10 ? FL_ALT + '0' + i : 0, 0);
  }

  if (i == 100)
    ((Fl_Menu_Item *)menu())[0].deactivate();
}


//
// 'FavoritesMenu::quote()' - Quote a filename for a menu item.
//

void
FavoritesMenu::quote(char       *dst,	// O - Destination string
                     const char *src,	// I - Source string
	             int        dstsize)// I - Size of destination string
{
  dstsize --;

  while (*src && dstsize > 1)
  {
    if (*src == '\\')
    {
      // Convert backslash to forward slash...
      *dst++ = '\\';
      *dst++ = '/';
      src ++;
    }
    else if (*src == '@')
    {
      // Quote @...
      *dst++ = '@';
      *dst++ = *src++;
    }
    else if (*src == '/')
    {
      // Quote /...
      *dst++ = '\\';
      *dst++ = *src++;
    }
    else
      *dst++ = *src++;
  }

  *dst = '\0';
}


//
// 'FavoritesMenu::unquote()' - Unquote a pathname from a menu.
//

void
FavoritesMenu::unquote(char       *dst,	// O - Destination string
                       const char *src,	// I - Source string
	               int        dstsize)
					// I - Size of destination string
{
  dstsize --;

  while (*src && dstsize > 1)
  {
    if (*src == '\\' || *src == '@')
      src ++;

    *dst++ = *src++;
  }

  *dst = '\0';
}


//
// End of "$Id: FavoritesMenu.cxx 386 2006-03-04 14:15:27Z mike $".
//

//
// "$Id: DiffOpenWindow.cxx 407 2006-11-13 18:54:02Z mike $"
//
// DiffOpenWindow widget code.
//
// Copyright 2005-2006 by Michael Sweet.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License v2 as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// Contents:
//
//   DiffOpenWindow::DiffOpenWindow() - Create an open/compare window.
//   DiffOpenWindow::compare_cb()     - Confirm the open/compare.
//   DiffOpenWindow::cancel_cb()      - Cancel the open/compare.
//   DiffOpenWindow::dc_cb()          - Handle selections in either chooser.
//   DiffOpenWindow::resize()         - Resize the window.
//   DiffOpenWindow::show()           - Show the window.
//

#include "DiffOpenWindow.h"


//
// 'DiffOpenWindow::DiffOpenWindow()' - Create an open/compare window.
//

DiffOpenWindow::DiffOpenWindow(
    const char *v1,			// I - First value
    const char *v2)			// I - Second value
  : Fl_Double_Window(640, 480, "Open/Compare"),
    dc1_(10, 25, 305, 410, "File/Directory 1:"),
    dc2_(325, 25, 305, 410, "File/Directory 2:"),
    compare_(435, 445, 120, 25, "Open/Compare"),
    cancel_(565, 445, 65, 25, "Cancel")
{
  end();
  modal();

  resizable(this);
  size_range(255, 225, Fl::w(), Fl::h()); 

  dc1_.align(FL_ALIGN_TOP_LEFT);
  dc1_.callback((Fl_Callback *)dc_cb, this);
  dc1_.labelfont(FL_HELVETICA_BOLD);
  dc1_.value(v1);

  dc2_.align(FL_ALIGN_TOP_LEFT);
  dc2_.callback((Fl_Callback *)dc_cb, this);
  dc2_.labelfont(FL_HELVETICA_BOLD);
  dc2_.value(v2);

  compare_.callback((Fl_Callback *)compare_cb, this);
  compare_.shortcut(FL_Enter);

  cancel_.callback((Fl_Callback *)cancel_cb, this);
}


//
// 'DiffOpenWindow::compare_cb()' - Confirm the open/compare.
//

void
DiffOpenWindow::compare_cb(
    Fl_Button      *b,			// I - Open/compare button
    DiffOpenWindow *dow)		// I - Diff open window
{
  dow->hide();
}


//
// 'DiffOpenWindow::cancel_cb()' - Cancel the open/compare.
//

void
DiffOpenWindow::cancel_cb(
    Fl_Button      *b,			// I - Cancel button
    DiffOpenWindow *dow)		// I - Diff open window
{
  dow->dc1_.deselect();
  dow->dc2_.deselect();

  dow->hide();
}


//
// 'DiffOpenWindow::dc_cb()' - Handle selections in either chooser.
//

void
DiffOpenWindow::dc_cb(
    DiffChooser    *dc,			// I - Diff chooser
    DiffOpenWindow *dow)		// I - Diff open window
{
  if (dow->dc1_.count())
  {
    dow->compare_.activate();

    if (Fl::event_clicks())
      dow->do_callback();
  }
  else
    dow->compare_.deactivate();
}


//
// 'DiffOpenWindow::resize()' - Resize the window.
//

void
DiffOpenWindow::resize(int X,		// I - X position
                       int Y,		// I - Y position
		       int W,		// I - Width
		       int H)		// I - Height
{
  Fl_Double_Window::resize(X, Y, W, H);

  dc1_.resize(10, 25, (W - 30) / 2, H - 70);
  dc2_.resize(20 + (W - 30) / 2, 25, (W - 30) / 2, H - 70);
  compare_.resize(W - 205, H - 35, 120, 25);
  cancel_.resize(W - 75, H - 35, 65, 25);
}


//
// 'DiffOpenWindow::show()' - Show the window.
//

void
DiffOpenWindow::show()
{
  if (dc1_.count())
    compare_.activate();
  else
    compare_.deactivate();

  hotspot(this);

  Fl_Double_Window::show();
}


//
// End of "$Id: DiffOpenWindow.cxx 407 2006-11-13 18:54:02Z mike $".
//

//
// "$Id: DiffOpenWindow.h 407 2006-11-13 18:54:02Z mike $"
//
// DiffOpenWindow widget definitions.
//
// Copyright 2005-2006 by Michael Sweet.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License v2 as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

#ifndef _DiffOpenWindow_h_
#  define _DiffOpenWindow_h_
#  include <FL/Fl.H>
#  include <FL/Fl_Double_Window.H>
#  include <FL/Fl_Button.H>
#  include <FL/Fl_Preferences.H>
#  include "DiffChooser.h"


class DiffOpenWindow : public Fl_Double_Window
{
  DiffChooser	dc1_, dc2_;		// Choosers
  Fl_Button	compare_,		// Open/Compare button
		cancel_;		// Cancel button

  static void	compare_cb(Fl_Button *b, DiffOpenWindow *dow);
  static void	cancel_cb(Fl_Button *b, DiffOpenWindow *dow);
  static void	dc_cb(DiffChooser *dc, DiffOpenWindow *dow);

  public:

		DiffOpenWindow(const char *v1 = (const char *)0,
		               const char *v2 = (const char *)0);
  DiffChooser	*dc1() { return (&dc1_); }
  DiffChooser	*dc2() { return (&dc2_); }
  void		resize(int X, int Y, int W, int H);
  void		show();
};


#endif // !_DiffOpenWindow_h_

//
// End of "$Id: DiffOpenWindow.h 407 2006-11-13 18:54:02Z mike $".
//
